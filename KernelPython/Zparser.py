class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y

    def __repr__(self):
        return "Point x={} y={}".format(self.x, self.y)

class SubCtr:
    def __init__(self, name, pointList):
        self.name = name
        self.pointList = pointList
    def __repr__(self):
        return self.name

class Zline:
    """Zline contains information about a line of the Z file

    """

    def __init__(self, line):
        self.register = ''
        self.values_list = []
        self.subprogram = ''
        self.call = ''
        
        if('=' in line):
            self.register = line[:line.index('=')]              # What is written before the first '='
            values_list_read = line[line.index('=')+1:].split() # What is written after the first '='
            # cast the value in float or remove the " if string
            for index, value in enumerate(values_list_read):
                try:
                    value = float(value)
                except :
                    value = value.replace('"', '')
                self.values_list.append(value)
        
        elif('SUB CTR' in line):
            self.subprogram = line[4:line.index('\n')] # get the name of the sub
        elif('CALL ' in line):
            self.call = line[5:line.index('(')] # get the name of the sub to call


def createSubprogram(z_file, sub_name):

    pointList = []
    line = z_file.readline()
    # We read the Z file line by line
    while not line.startswith('ENDSUB'):
        z_line = Zline(line)
        if(z_line.register=='POS'):
            point = Point(*z_line.values_list)
            pointList.append(point)
        elif(z_line.register=='X'):
            point = Point(z_line.values_list[0], pointList[len(pointList)-1].y)
            pointList.append(point)
        elif(z_line.register=='Y'):
            point = Point(pointList[len(pointList)-1].x, z_line.values_list[0])
            pointList.append(point)
        line = z_file.readline()

    return SubCtr(sub_name, pointList)


class Call:
    def __init__(self, sub_name, call_point):
        self.sub_name=sub_name
        self.call_point=call_point

    def __repr__(self):
        return str(self.sub_name) + " at " + str(self.call_point)


class Material:
    """Material contains information about the material of the sheet of the nesting

    """

    def __init__(self, name='', thickness=0.0, density = 0.0):
        self.name=name
        self.thickness=thickness
        self.density=density

    def __repr__(self):
        return self.name + ", thickness=" + str(self.thickness)


class Sheet:
    """Sheet contains information about the cut sheet

    material is the material of the sheet, default value is None
    length the length of the sheet as a double, default value is 0.0
    width is the width of the sheet as a double containing, default value is 0.0
    """

    def __init__(self, material=Material(), length=0.0, width=0.0, weight=0):
        self.material = material
        self.length = length
        self.width = width
        self.weight = weight

    def __repr__(self):
        return "{}x{} in {}".format(self.length, self.width, self.material)


class Program:
    """Program contains all available information about the cutting program


    """
    
    def __init__(self, z_file_path):
        """Build the program object by reading the Z file

        z_file_path is a string containing the path to the Z file
        """

        self.main_program = []
        self.sub_program_ctr = {}
        self.z_file_path = z_file_path
        start_main = False
        # We open the Z file
        with open(z_file_path) as z_file:  
            line = z_file.readline()
            # We read the Z file line by line
            while line:
                z_line = Zline(line)

                if(z_line.register=='SHEET'):
                    material = Material(z_line.values_list[7], z_line.values_list[4], z_line.values_list[8])
                    self.sheet = Sheet(material, z_line.values_list[2], z_line.values_list[3], z_line.values_list[9])
                elif(line=='INI\n'):
                    start_main = True
                elif(z_line.subprogram!=''):
                    subprogram = createSubprogram(z_file, z_line.subprogram)
                    self.sub_program_ctr[z_line.subprogram] = subprogram
                elif(start_main and z_line.register=='POS'):
                    sub_call_point = Point(*z_line.values_list)
                elif(start_main and z_line.call!=''):
                    self.main_program.append(Call(self.sub_program_ctr[z_line.call], sub_call_point))

                line = z_file.readline()