import Zparser
import tempfile
import sys
from pathlib import Path
import ctypes


# Mettre PFI_167=1 pour sortie Python
#
# commande line to run the post pro:
#"C:\Users\arnoux\Documents\CloneGit\KernelPython\KernelPython\postProcessor.py"
#
# Post-processeur:
#ON ENDPGM
#	SYSTEM("C:\Users\arnoux\Documents\CloneGit\KernelPython\KernelPython\postProcessor.py "+ARGV(1))
#END
#
#Message box:
# ctypes.windll.user32.MessageBoxW(0, "coucou", "Your title", 1)

def addFileToFilelist(cn_filename, z_file_path): #todo make it stronger
    filelist_path = z_file_path.replace('.z', '.g') #weak if the name contains a .z
    filelist_file = open(filelist_path, 'w')
    filelist_file.writelines(Path(cn_filename).name + '\n') #weak if several cn file with same name
    filelist_file.writelines(cn_filename)
    filelist_file.close()

def computeCnFilename(program):
    return tempfile.gettempdir() + "\\" + str(program.sheet.material) + ".cn"

def printHeader(program, cn_file):
    cn_file.write('* Material=' + str(program.sheet.material) + '\n')
    cn_file.write('* Sheet=' + str(program.sheet) + '\n')

def printMainProgram(program, cn_file):
    cn_file.write('\n')
    cn_file.writelines('* Start Main program' + '\n')
    for call in program.main_program:
        cn_file.writelines('G00 X{} Y{}'.format(call.call_point.x, call.call_point.y) + '\n')
        cn_file.writelines('GOTO ' + str(call.sub_name) + '\n')
    cn_file.writelines('* End Main program' + '\n')
    cn_file.writelines('\n')

def printSubPrograms(program, cn_file):
    cn_file.writelines('\n')
    cn_file.writelines('* Start Sub programs' + '\n')
    for (key, subCtr) in program.sub_program_ctr.items():
        cn_file.writelines(subCtr.name + '\n')
        for point in subCtr.pointList:
            cn_file.writelines('G00 X{} Y{}'.format(point.x, point.y) + '\n')
    cn_file.writelines('* End Sub programs' + '\n')

def executePostProcessor(program):
    cn_filename = computeCnFilename(program)
    cn_file = open(cn_filename, 'w')
    printHeader(program, cn_file)
    printMainProgram(program, cn_file)
    printSubPrograms(program, cn_file)
    cn_file.close()
    return cn_filename


program = Zparser.Program(sys.argv[1]) #sys.argv[1]) #Retrieve the z file from the call "C:/Data39/Laser/plac/kernelpython.z01") #
cn_filename = executePostProcessor(program)
addFileToFilelist(cn_filename, program.z_file_path) #so that the cn file is imported in database
print(program)